db.fruits.aggregate([
		{$match: {$and:[{supplier: "Yellow Farms"}, {price:{$lt: 50}}]}},
		{$count: "priceLowerThan50YellowFarms"}
	])

db.fruits.aggregate([
	{$match:{price:{$lt:30}}},
	{$count: "lessThan30"}

])

db.fruits.aggregate([
		{$match:{supplier: "Yellow Farms"}},
		{$group:{_id:"$supplier", avgPrice: {$avg:"$price"}}}
	])

db.fruits.aggregate([
		{$match:{supplier: "Red Farms Inc."}},
		{$group:{_id:"$supplier", maxPrice: {$max:"$price"}}}
	])
db.fruits.aggregate([
		{$match:{supplier: "Red Farms Inc."}},
		{$group:{_id:"$supplier", minPrice: {$min:"$price"}}}
	])
