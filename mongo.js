db.fruits.insertMany([
{
	name:"Apple",
	supplier: "Red Farms Inc.",
	stocks: 20,
	price: 40,
	onSale: true
},
{
	name:"Banana",
	supplier: "Yellow Farms",
	stocks: 15,
	price: 20,
	onSale: true
},
{
	name:"Kiwi",
	supplier: "Green Farming and Canning",
	stocks: 25,
	price: 50,
	onSale: true
},
{
	name:"Mango",
	supplier: "Yellow Farms",
	stocks: 10,
	price: 60,
	onSale: true
},
{
	name:"Dragon  Fruit",
	supplier: "Red Farms Inc.",
	stocks: 10,
	price: 60,
	onSale: true
},
])

//Aggregation Pipeline Stages
//Aggregation is typiocally done in 2-3 steps. Each process in aggregation is called a stage
// $match - is used to match or get documents which satisfies the condition
//syntax: {$match: {field:<value>}}

//$group - allows us to group together documents and create and analysis

//_id: in the group stage, essentially associates an id to our results.
//_id: also determines the number of groups.



db.fruits.aggregate([

	//looked for  and got all fruits that are onSale
	{$match: {onSale:true}}, //apple, banana, mango, kiwi, dragon fruit

	/*
		apple
		supplier: Red Farms Inc

		Banana
		supplier: Yellow Farms

		Mango
		supplier: Yellow Farms

		Kiwi
		supplier: Green Farming

		Dragon Fruit:
		supplier: Red Farms Inc.

		
		group1
		_id: Red Farms Inc
		apple
		dragon fruit

		group2
		_id: Yellow Farms
		banana
		mango

		group3
		_id: Green Farming
		Kiwi

		$sum is used to add or total the values of the given field

		group1
		apple
		stocks:20
		dragon fruit
		stocks: 10
		$sum: 30

		group2
		banana
		stocks: 15
		mango
		stocks: 10
		$sum: 25

		group3
		kiwi 
		stocks: 25
		$sum: 25



	*/
	//totalStocks can be named anything
	{$group: {_id:"$supplier", totalStocks:{$sum:"$stocks"}}}

	])

db.fruits.aggregate([

	//If the _id's value is definite or given, $group will only create on group
	{$match: {onSale:true},
	{$group: {_id:null, totalStocks:{$sum: "$stocks"}}}
	])


//$avg - is an operator used in $group stage
//$avg gets the avg of the numerical values of the indicated field in grouped documents
db.fruits.aggregate([

	{$match:{onSale:true}},
	{$group:{_id: "$supplier", avgStock: {$avg: "$stocks"}}}
	])}

//$max - will allow us to get the highest value out of all the values in a given field per group.
//highest number of stock for all items on sale.

db.fruits.aggregate(
	[
		{$match: {onSale:true}},
		{$group: {_id: "highestStockOnSale", maxStock:{$max: "$stocks"}}}

	]

	)

db.fruits.aggregate([
	{$match: {onSale:true}},
	{$group: {_id:null, maxPrice: {$max: "$price"}}}

	])

//$min - gets the lowest value of the values in a given field per group.

//get the lowest number of stock for all items on sale
db.fruits.aggregate([
	{$match: {onSale:true}},
	{$group: {_id:lowestStockOnSale, minStock: {$min: "$stock"}}}
	])

//lowest price
db.fruits.aggregate([
{$match: {price:{$lt: 50}},
{$group: {_id:"lowestPriceOnSale", minPrice: {$min: "$price"}}}
	])

//Other stages
//$count - is a stage added after $match stage to count all items that matches our criteria

//counts all items on sale
db.fruits.aggregate([
{$match: {onSale:true}},
{$count: "itemsOnSale"}
	])


db.fruits.aggregate([
	{$match:{price:{$lt:50}}},
	{$count: "itemsPriceLessThan50"
])

//number of Items with stocks less than 20
db.fruits.aggregate([
		{$match: {stocks:{$lt: 20}}},
		{count: "forRestock"}
	])

//$out - save,output the result in a new collection
//note: This will overwrite the collection if it already exist
db.fruits.aggregate([
		{$match:{onSale:true}},
		{$group:{_id:"Supplier", totalStocks: {$sum: "$stocks"}}},
		{$out: "stocksPerSupplier"}
	])